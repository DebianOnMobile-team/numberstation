import argparse
import os
import gi
import sys

from numberstation.window import NumberstationWindow

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio

gi.require_version('Handy', '1')
from gi.repository import Handy


class NumberstationApplication(Gtk.Application):
    def __init__(self, application_id, flags, version):
        Gtk.Application.__init__(self, application_id=application_id, flags=flags)
        self.connect("activate", self.open_window)
        self.connect("open", self.open_url)
        self.version = version
        self.window = None

    def open_window(self, *args):
        if self.window is None:
            self.window = NumberstationWindow(self, self.version)
        self.window.present()

    def open_url(self, app, files, *hint):
        self.open_window()

        for file in files:
            self.window.import_url(file.get_uri())

        self.window.build_code_list()

def main(version):
    Handy.init()

    if os.path.isfile('numberstation/numberstation.gresource'):
        print("Using resources from cwd/numberstation")
        resource = Gio.resource_load("numberstation/numberstation.gresource")
        Gio.Resource._register(resource)
    elif os.path.isfile('numberstation.gresource'):
        print("Using resources from cwd")
        resource = Gio.resource_load("numberstation.gresource")
        Gio.Resource._register(resource)

    app = NumberstationApplication("org.postmarketos.Numberstation", Gio.ApplicationFlags.HANDLES_OPEN,
                                   version=version)
    app.run(sys.argv)


if __name__ == '__main__':
    main('development')
